import argparse
from sys import argv
from .builder import build_project

def main():

    parser = argparse.ArgumentParser(
        prog="Hephaestus",
        description="An e-multitool for building, formatting, and testing projects."
    )

    # Create a group of build-related arguments
    build_group = parser.add_argument_group(title="building", 
                                            description="Commands for building the project")
    
    build_group.add_argument('-b', '--build', type=str, choices=["release", "debug"],
                             help="Build a project with specified type."
                             )
    
    build_group.add_argument('-c', '--clean', action="store_true", 
                             help="Perform a clean build of the project.")
    
    args = parser.parse_args()

    if not len(argv) > 1:
        parser.print_help()
        exit(0)

    if args.build:
        build_project(args.build, args.clean)

if __name__ == "__main__":
    main()
