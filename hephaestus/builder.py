from subprocess import Popen
from typing import List
from os.path import dirname, realpath
import json

def build_project(build_type: str, clean: bool) -> None:
    _setup_preset_file()

    # Define arguments for cmake calls
    setup_args = [f"cmake",
                  f"--preset",
                  f"{build_type}"]
    
    build_args = [f"cmake",
                  f"--build",
                  f"--preset",
                  f"build-{build_type}",
                  f"--target",
                  f"install"]
    
    if clean:
        build_args.append("--clean-first")
    
    setup = Popen(setup_args)
    setup.communicate()

    build = Popen(build_args)
    build.communicate()

def _setup_preset_file() -> None:
    print("Hephaestus requires you to use its CMake presets to build your project.")
    print("Checking if you have have a presets file...")

    try:
        with open("CMakePresets.json", "r") as file:
            presets = json.load(file)

        print("Found CMakePresets.json file.")

        if 'include' in presets:
            if _contains_builds_json(presets['include']):
                print("Found include for Hephaestus presets.")
                return

        print("Found CMakePresets.json does not include Hephaestus presets.")
        resp = input("Do you want to add it? [Y/n] ")
        
        if resp in ("y", "Y", "yes", "Yes", "YES", ""):
            _append_preset_path(presets)
        
        else:
            print("You must include Hephaestus's preset file to use Hephaestus's building functions.")
            exit(1)
        
    
    except FileNotFoundError:
        print("Did not find CMakePresets.json file.")
        resp = input("Do you want to create one? [Y/n] ")

        if resp in ("y", "Y", "yes", "Yes", "YES", ""):
            _generate_presets()
        
        else:
            print("You must have a CMakePresets.json file to use Hephaestus's building functinons.")
            exit(1)

def _append_preset_path(presets: List[str]) -> None:
    include_path = realpath(f"{dirname(dirname(realpath(__file__)))}/builds.json")

    try:
        # Append to 'include' field
        paths = presets["include"]
        paths += include_path
        presets["include"] = paths

    except KeyError:
        # Create an 'include' field
        presets['include'] = [include_path]

    # We can safely do this, since we know the file exists already
    with open("CMakePresets.json", "w") as file:
        json.dump(presets, file, indent=4)

    
def _generate_presets():
    print("Creating presets file for build process...")

    include_path = realpath(f"{dirname(dirname(realpath(__file__)))}/builds.json")

    file_info = {
        "version": 7,
        "cmakeMinimumRequired": {
            "major": 3,
            "minor": 28,
            "patch": 0
        },
        "include": [include_path]
    }

    json_obj = json.dumps(file_info, indent=4)
    with open("CMakePresets.json", "w") as out_file:
        out_file.write(json_obj)

def _contains_builds_json(include_paths: List[str]) -> bool:
    build_path = realpath(f"{dirname(dirname(realpath(__file__)))}/builds.json")
    return build_path in (realpath(p) for p in include_paths)