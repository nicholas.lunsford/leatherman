from setuptools import setup

setup(
    name = 'hephaestus',
    version = '0.1.0',
    author = "Nicholas Lunsford",
    description = "An e-multitool for building, formatting, and testing projects.",
    packages = ['hephaestus'],
    entry_points = {
        'console_scripts': [
            'hephaestus = hephaestus.__main__:main'
        ]
    })