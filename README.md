# Hephaestus

**Hephaestus** is an e-multitool meant to build, format, and test projects using CMake as a buildsystem. It improves quality of life for developers by abstracting the various commands and programs used in the development process (such as `cmake`, `clang-tidy`, `black`,
`clang-format`, `ctest`, etc.) into one, command-line program. 

Hephaestus is designed to be built and installed locally on the developer's system, but could also be imported as a `git` submodule in another project.

As a simple wrapper and QoL improvement tool, any project using Hephaestus should also be able to build, format, and test on its own without depending on Hephaestus.

## Installation

To build Hephaestus and use it as a command line program, run the following command from the project directory.

```console
$ pip3 install -e .
```

It may be desirable to add Hephaestus to your system's path, as `pip` places it in `~/.local/bin`. To do so, run the following command and restart your shell:

```console
$ export PATH="$HOME/.local/bin:$PATH"
```

## Features

### Building

Hephaestus can build projects that use CMake as a build system. To do this, Hephaestus holds a `CMakePresets.json` file that defines parameters for a `build/` directory, installation directory, and various `CMakeCache` variable values for release, debug, and test builds.

Currently, Hephaestus only uses the presets it has already defined. As a result, if your project already contains a `CMakePresets.json` file in its top level directory, it **must** include Hephaestus's `builds.json` file. Hephaestus will automatically check and try to add itself to an existing `CMakePresets.json`, or create one for you.

Hephaestus currently supports only `debug` and `release` build types.

### Usage

| Option | Description |
| :---   | :---        |
|`--build [type]`, `-b [type]`| Required command to initiate a new build. Must specify the type of build to run. |
|`--clean`, `-c`| Perform a clean build |

### Formatting

Formatting commands have not yet been implemented. Future work will wrap `clang-format` and `black` commands to auto-format a project.

### Testing

Testing commands have not yet been implemented. Future work will integrate `ctest` and `pytest` wrapping capabilities.

## Contributing

Hephaestus is not currently open to external contribution.

